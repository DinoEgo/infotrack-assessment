﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InfoTrack.Models;
using System.Net;
using System.IO;
using System.Text;

namespace InfoTrack.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult FindURLKeyWords([Bind("url, keyword")] UrlSearch search)
        {
            //check if url to be searched is empty
            if (string.IsNullOrEmpty(search.url))
            {
                return BadRequest();
            }
            //set the keyword to empty string if empty
            if (string.IsNullOrEmpty(search.keyword))
            {
                search.keyword = "";
            }
            //help from: https://stackoverflow.com/questions/16642196/get-html-code-from-website-in-c-sharp
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(search.url);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            //ensure the web request is OK
            if (res.StatusCode == HttpStatusCode.OK)
            {
                //get datastream
                Stream resStream = res.GetResponseStream();
                //create reader to read stream
                StreamReader reader = new StreamReader(resStream, Encoding.GetEncoding(res.CharacterSet));
                //create string of data stream
                string data = reader.ReadToEnd();

                //close the connection/reader
                res.Close();
                reader.Close();

                //change to lowercase to improve searching
                data = data.ToLower();
                search.keyword = search.keyword.ToLower();

                //data = data.Substring(data.LastIndexOf("</style>"));


                int count = 0;
                List<string> linkArray = new List<string>();
                //search if data contains the keyword
                if (data.Contains(search.keyword))
                {
                    while (data.Contains(search.keyword.ToLower()))
                    {
                        //find the beginning of a link
                        int startIndex = data.IndexOf("<a href=\"/url?q=http");
                        //if not found skip over loop
                        if (startIndex < 0) continue;

                        //isolate the link
                        string linkTag = data.Substring(startIndex);
                        int endIndex = linkTag.IndexOf("</a>") + 4;
                        linkTag = linkTag.Substring(0, endIndex);
                        //create an exception for google links
                        if (linkTag.Contains(search.keyword) && !linkTag.Contains("google"))
                        {
                            //increment count and fix url for redirection
                            count++;
                            linkTag = linkTag.Replace("/url?q=", "https://www.google.co.uk/url?q=");
                            //add to linkArray
                            linkArray.Add("<tr><th scope='row'>" + count + "</th><td>" + linkTag + "</td></tr>");
                        }
                        //move the datastream to end of found link to move on
                        data = data.Substring(startIndex + endIndex);
                    }
                }
                ViewData["count"] = count;
                ViewData["linkArray"] = linkArray;
                ViewData["keyword"] = string.IsNullOrEmpty(search.keyword) ? "'anything'" : "'" + search.keyword + "'";
                ViewData["url"] = "'" + search.url + "'";
                return View();
            }
            return BadRequest();
        }
    }

    public class UrlSearch
    {
        public string url { get; set; }
        public string keyword { get; set; }
    }
}
